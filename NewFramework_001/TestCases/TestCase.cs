﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewFramework_001.UIControls.Word;
using NewFramework_001.UIControls.Excel;
using NewFramework_001.UIControls.CommandCentre;
using System;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace NewFramework_001.TestCases
{

    /// <summary>
    /// Summary description for testcases
    /// </summary>
    [CodedUITest]
    public class Test_Case
    {
        private WordConnection _wordConnection;
        private WordDisconnect _wordDisconnect;
        private ExcelConnection _ExcelConnect;
        private ExcelDisconnection _ExcelDisconnectionMethod;
        private TC001 _OpenClinNegMatterFEPH;



        public Test_Case()
        {
            _wordConnection = new WordConnection();
            _wordDisconnect = new WordDisconnect();
            _ExcelConnect = new ExcelConnection();
            _ExcelDisconnectionMethod = new ExcelDisconnection();
            _OpenClinNegMatterFEPH = new TC001();

        }

        [TestMethod]
        public void TestCaseB()
        {
            //open word connect to environment open mattersphere ribbon
            this._wordConnection.openwordconnection();

            //TC001 - open command centre, fee earner Paul Holder search for clin neg matter and open
            this._OpenClinNegMatterFEPH.TC_001();

            //disconnect from mattersphere close word
            _wordDisconnect.closeword();
           
        }


        [TestMethod]
        public void testcase001()
        {
            //open excel connect to environment open mattersphere
            this._ExcelConnect.ExcelConnect();

            //disconnect from mattersphere close word
            this._ExcelDisconnectionMethod.ExcelDisconnectionMethod();

        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }

}