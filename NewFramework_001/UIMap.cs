﻿namespace NewFramework_001
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using NewFramework_001;


    public partial class UIMap
    {

        /// <summary>
        /// WordConnection - Use 'WordConnectionParams' to pass parameters into this method.
        /// </summary>
        public void WordConnection()
        {
            #region Variable Declarations
            WinListItem uIBlankdocumentListItem = this.UIWordWindow.UIFeaturedList.UIBlankdocumentListItem;
            WinButton uIConnectButton = this.UIWordWindow.UIItemWindow.UIMatterSphereToolBar.UIConnectButton;
            WinComboBox uICboMultiDBComboBox = this.UIMatterSphereWindow.UICboMultiDBWindow.UICboMultiDBComboBox;
            WinCheckBox uIClearCacheCheckBox = this.UIMatterSphereWindow.UIClearCacheWindow.UIClearCacheCheckBox;
            WinButton uILoginButton = this.UIMatterSphereWindow.UILoginWindow.UILoginButton;
            WinTabPage uIMatterSphereTabPage = this.UIWordWindow.UIItemWindow.UIRibbonClient.UIMatterSphereTabPage;
            #endregion


        }

        /// <summary>
        /// WordDisconnect
        /// </summary>
        public void WordDisconnect()
        {
            #region Variable Declarations
            WinTabPage uIHomeTabPage = this.UIWordWindow.UIItemWindow.UIRibbonClient.UIHomeTabPage;
            WinButton uIDisconnectButton = this.UIWordWindow.UIItemWindow.UIMattersphereTestToolBar.UIDisconnectButton;
            WinButton uICloseButton = this.UIWordWindow.UIItemWindow.UIRibbonPropertyPage.UICloseButton;
            #endregion

            // Click 'Home' tab
            Mouse.Click(uIHomeTabPage, new Point(28, 9));

            // Click 'Disconnect' button
            Mouse.Click(uIDisconnectButton, new Point(38, 19));

            // Click 'Close' button
            Mouse.Click(uICloseButton, new Point(10, 7));
        }


        /// <summary>
        /// wordconnection - Use 'wordconnectionParams' to pass parameters into this method.
        /// </summary>
        public void wordconnection()
        {
            #region Variable Declarations
            WinListItem uIBlankdocumentListItem = this.UIWordWindow.UIFeaturedList.UIBlankdocumentListItem;
            WinButton uIConnectButton = this.UIWordWindow.UIItemWindow.UIMatterSphereToolBar.UIConnectButton;
            WinComboBox uICboMultiDBComboBox = this.UIMatterSphereWindow.UICboMultiDBWindow.UICboMultiDBComboBox;
            WinCheckBox uIClearCacheCheckBox = this.UIMatterSphereWindow.UIClearCacheWindow.UIClearCacheCheckBox;
            WinButton uILoginButton = this.UIMatterSphereWindow.UILoginWindow.UILoginButton;
            WinTabPage uIMatterSphereTabPage = this.UIWordWindow.UIItemWindow.UIRibbonClient.UIMatterSphereTabPage;
            #endregion

            // Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
            ApplicationUnderTest wINWORDApplication = ApplicationUnderTest.Launch(this.wordconnectionParams.ExePath, this.wordconnectionParams.AlternateExePath);

            // Click 'Blank document' list item
            Mouse.Click(uIBlankdocumentListItem, new Point(81, 176));

            // Click 'Connect' button
            Mouse.Click(uIConnectButton, new Point(33, 20));

            // Select 'Mattersphere Test' in 'cboMultiDB' combo box
            uICboMultiDBComboBox.SelectedItem = KnownEnvironment.GetEnvironment();

            // Select 'Clear &Cache' check box
            uIClearCacheCheckBox.Checked = this.wordconnectionParams.UIClearCacheCheckBoxChecked;

            // Click 'Login' button
            Mouse.Click(uILoginButton, new Point(36, 14));

            // Click 'MatterSphere®' tab
            Mouse.Click(uIMatterSphereTabPage, new Point(43, 13));
        }

        public virtual wordconnectionParams wordconnectionParams
        {
            get
            {
                if ((this.mwordconnectionParams == null))
                {
                    this.mwordconnectionParams = new wordconnectionParams();
                }
                return this.mwordconnectionParams;
            }
        }

        private wordconnectionParams mwordconnectionParams;

        /// <summary>
        /// ExcelConnect - Use 'ExcelConnectParams' to pass parameters into this method.
        /// </summary>
        public void ExcelConnect()
        {
            #region Variable Declarations
            WinListItem uIBlankworkbookListItem = this.UIExcelWindow.UIFeaturedList.UIBlankworkbookListItem;
            WinButton uIConnectButton = this.UIBook1ExcelWindow.UIItemWindow.UIMatterSphereToolBar.UIConnectButton;
            WinComboBox uICboMultiDBComboBox = this.UIMatterSphereWindow.UICboMultiDBWindow.UICboMultiDBComboBox;
            WinCheckBox uIClearCacheCheckBox = this.UIMatterSphereWindow.UIClearCacheWindow.UIClearCacheCheckBox;
            WinButton uILoginButton = this.UIMatterSphereWindow.UILoginWindow.UILoginButton;
            WinTabPage uIMatterSphereTabPage = this.UIBook1ExcelWindow.UIItemWindow.UIRibbonClient.UIMatterSphereTabPage;
            #endregion

            // Launch '%ProgramW6432%\Microsoft Office 15\root\office15\excel.exe'
            ApplicationUnderTest excelApplication = ApplicationUnderTest.Launch(this.ExcelConnectParams.ExePath, this.ExcelConnectParams.AlternateExePath);

            // Click 'Blank workbook' list item
            Mouse.Click(uIBlankworkbookListItem, new Point(100, 108));

            // Click 'Connect' button
            Mouse.Click(uIConnectButton, new Point(18, 30));

            // Select 'Mattersphere Test' in 'cboMultiDB' combo box
            uICboMultiDBComboBox.SelectedItem = KnownEnvironment.GetEnvironment();

            // Select 'Clear &Cache' check box
            uIClearCacheCheckBox.Checked = this.ExcelConnectParams.UIClearCacheCheckBoxChecked;

            // Click 'Login' button
            Mouse.Click(uILoginButton, new Point(41, 11));

            // Click 'MatterSphere®' tab
            Mouse.Click(uIMatterSphereTabPage, new Point(51, 10));
        }

        public virtual ExcelConnectParams ExcelConnectParams
        {
            get
            {
                if ((this.mExcelConnectParams == null))
                {
                    this.mExcelConnectParams = new ExcelConnectParams();
                }
                return this.mExcelConnectParams;
            }
        }

        private ExcelConnectParams mExcelConnectParams;

        /// <summary>
        /// ExcelDisconnection
        /// </summary>
        public void ExcelDisconnection()
        {
            #region Variable Declarations
            WinTabPage uIHomeTabPage = this.UIBook1ExcelWindow.UIItemWindow.UIRibbonClient.UIHomeTabPage;
            WinButton uIDisconnectButton = this.UIBook1ExcelWindow.UIItemWindow.UIMattersphereTestToolBar.UIDisconnectButton;
            WinButton uICloseButton = this.UIBook1ExcelWindow.UIItemWindow.UIRibbonPropertyPage.UICloseButton;
            WinButton uIOKButton = this.UIMatterSphereWindow1.UIOKWindow.UIOKButton;
            WinButton uIDontSaveButton = this.UIMicrosoftExcelWindow.UIMicrosoftExcelDialog.UIDontSaveButton;
            #endregion

            // Click 'Home' tab
            Mouse.Click(uIHomeTabPage, new Point(36, 8));

            // Click 'Disconnect' button
            Mouse.Click(uIDisconnectButton, new Point(33, 25));

            // Click 'Close' button
            Mouse.Click(uICloseButton, new Point(11, 10));

            // Click 'OK' button
            Mouse.Click(uIOKButton, new Point(29, 14));

            // Click 'Don't Save' button
            Mouse.Click(uIDontSaveButton, new Point(48, 15));
        }

        /// <summary>
        /// OpenClinNegMatterFEPH - Use 'OpenClinNegMatterFEPHParams' to pass parameters into this method.
        /// </summary>
       public void OpenClinNegMatterFEPH()
        {
            #region Variable Declarations
            WinButton uICommandCentreButton = this.UIWordWindow.UIItemWindow.UIOMSViewToolBar.UICommandCentreButton;
            WinTabPage uIMatterListTabPage = this.UICommandCentrePaulHolWindow.UITcEnquiryPagesWindow.UIMatterListTabPage;
            WinComboBox uIComboBox1ComboBox = this.UICommandCentrePaulHolWindow.UIComboBox1Window.UIComboBox1ComboBox;
            WinComboBox uIComboBox1ComboBox1 = this.UICommandCentrePaulHolWindow.UIComboBox1Window1.UIComboBox1ComboBox;
            WinComboBox uIComboBox1ComboBox2 = this.UICommandCentrePaulHolWindow.UIComboBox1Window2.UIComboBox1ComboBox;
            WinButton uISearchButton = this.UICommandCentrePaulHolWindow.UISearchWindow.UISearchButton;
            WinCell uIPaulHolderCell = this.UICommandCentrePaulHolWindow.UIDgSearchResultsWindow.UIDataGridTable.UIUM1330582Row.UIPaulHolderCell;
            WinMenuItem uIViewMatterMenuItem = this.UIItemWindow.UIContextMenu.UIViewMatterMenuItem;
            WinButton uIOKButton = this.UICommandCentrePaulHolWindow.UITbRightWindow.UIOKButton;
            #endregion

            // Double-Click 'Command Centre' button
            Mouse.Click(uICommandCentreButton, new Point(37, 26));

            // Click 'Matter List' tab
            Mouse.Click(uIMatterListTabPage, new Point(51, 12));

            // Select 'paul holder' in 'comboBox1' combo box
            uIComboBox1ComboBox.SelectedItem = this.OpenClinNegMatterFEPHParams.UIComboBox1ComboBoxSelectedItem;

            // Select 'Clinical & Medical Negligence' in 'comboBox1' combo box
            uIComboBox1ComboBox1.SelectedItem = this.OpenClinNegMatterFEPHParams.UIComboBox1ComboBoxSelectedItem1;

            // Select 'Clinical Negligence' in 'comboBox1' combo box
            uIComboBox1ComboBox2.SelectedItem = this.OpenClinNegMatterFEPHParams.UIComboBox1ComboBoxSelectedItem2;

            // Click '&Search' button
            Mouse.Click(uISearchButton, new Point(21, 13));

            // Click 'Paul Holder' cell
            Mouse.Click(uIPaulHolderCell, new Point(36, 10));

            // Right-Click 'Paul Holder' cell
            Mouse.Click(uIPaulHolderCell, MouseButtons.Right, ModifierKeys.None, new Point(36, 10));

            // Click 'View Matter' menu item
            Mouse.Click(uIViewMatterMenuItem, new Point(22, 12));

            //wait for 8secs
            System.Threading.Thread.Sleep(10000);

            // Click 'OK' button
            Mouse.Click(uIOKButton, new Point(29, 14));

            //wait for 8secs
            System.Threading.Thread.Sleep(8000);
        }

        public virtual OpenClinNegMatterFEPHParams OpenClinNegMatterFEPHParams
        {
            get
            {
                if ((this.mOpenClinNegMatterFEPHParams == null))
                {
                    this.mOpenClinNegMatterFEPHParams = new OpenClinNegMatterFEPHParams();
                }
                return this.mOpenClinNegMatterFEPHParams;
            }
        }

        private OpenClinNegMatterFEPHParams mOpenClinNegMatterFEPHParams;
    }
    /// <summary>
    /// Parameters to be passed into 'wordconnection'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class wordconnectionParams
    {

        #region Fields
        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
        /// </summary>
        public string ExePath = "C:\\Program Files\\Microsoft Office 15\\root\\office15\\WINWORD.EXE";

        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\WINWORD.EXE'
        /// </summary>
        public string AlternateExePath = "%ProgramW6432%\\Microsoft Office 15\\root\\office15\\WINWORD.EXE";

        /// <summary>
        /// Select 'Mattersphere Test' in 'cboMultiDB' combo box
        /// </summary>
        public string UICboMultiDBComboBoxSelectedItem = "Mattersphere Test";

        /// <summary>
        /// Select 'Clear &Cache' check box
        /// </summary>
        public bool UIClearCacheCheckBoxChecked = true;
        #endregion
    }
    /// <summary>
    /// Parameters to be passed into 'ExcelConnect'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]
    public class ExcelConnectParams
    {

        #region Fields
        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\excel.exe'
        /// </summary>
        public string ExePath = "C:\\Program Files\\Microsoft Office 15\\root\\office15\\excel.exe";

        /// <summary>
        /// Launch '%ProgramW6432%\Microsoft Office 15\root\office15\excel.exe'
        /// </summary>
        public string AlternateExePath = "%ProgramW6432%\\Microsoft Office 15\\root\\office15\\excel.exe";

        /// <summary>
        /// Select 'Mattersphere Test' in 'cboMultiDB' combo box
        /// </summary>
        public string UICboMultiDBComboBoxSelectedItem = "Mattersphere Test";

        /// <summary>
        /// Select 'Clear &Cache' check box
        /// </summary>
        public bool UIClearCacheCheckBoxChecked = true;
        #endregion
    }
    /// <summary>
    /// Parameters to be passed into 'OpenClinNegMatterFEPH'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "14.0.23107.0")]

    

        public class OpenClinNegMatterFEPHParams
        {
            
            public string UIComboBox1ComboBoxSelectedItem = "Paul Holder";

            public string UIComboBox1ComboBoxSelectedItem1 = "Clinical & Medical Negligence";

            public string UIComboBox1ComboBoxSelectedItem2 = "Clinical Negligence";
        }
    }



